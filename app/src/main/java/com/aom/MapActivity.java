package com.aom;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.aom.fragments.HomeFragment;
import com.aom.fragments.MapFragment;
import com.aom.models.AmbulanceDetail;
import com.aom.parse.HttpRequester;
import com.aom.parse.ParseContent;
import com.aom.utils.AndyUtils;
import com.aom.utils.Const;
import com.aom.utils.PreferenceHelper;
import com.google.android.gms.maps.model.LatLng;
import com.google.zxing.client.android.CaptureActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;


public class MapActivity extends BaseActivity {

    public AmbulanceDetail ambulanceDetail;
    private Timer timer;
    private boolean isFirstCall = true;
    private PreferenceHelper pHelper;
    private ParseContent pContent;
    private ImageView ivNavigation, ivAmbulance;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        LinearLayout llBottom = (LinearLayout) findViewById(R.id.llBottom);
        assert llBottom != null;
        ivAmbulance = (ImageView) llBottom.findViewById(R.id.ivAmbulance);
        ivAmbulance.setOnClickListener(this);
        ivNavigation = (ImageView) llBottom.findViewById(R.id.ivNavigation);
        ivNavigation.setOnClickListener(this);
        llBottom.findViewById(R.id.ivLogout).setOnClickListener(this);
        pHelper = new PreferenceHelper(this);
        pContent = new ParseContent(this);

        if(findViewById(R.id.contentFrame1) == null) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        else{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        AndyUtils.showCustomProgressDialog(this, false);
        ivNavigation.setAlpha(0.6f);
        ivAmbulance.setAlpha(1.0f);
        startUpdatingAmbulanceDetail();
    }

    public void startUpdatingAmbulanceDetail(){
        timer = new Timer();
        timer.scheduleAtFixedRate(new AmbDetailTimerTask(), Const.DELAY, Const.TIME_SCHEDULE);
    }

    private class AmbDetailTimerTask extends TimerTask {
        @Override
        public void run() {
            getAmbulanceDetail();
        }
    }

    private void getAmbulanceDetail(){
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.URL, Const.ServiceType.CHECK_AMBULANCE_STATUS);
        map.put(Const.Params.ID, String.valueOf(pHelper.getUserId()));
        map.put(Const.Params.TYPE, String.valueOf(pHelper.getLoginType()));
        new HttpRequester(this, map, Const.ServiceCode.CHECK_AMBULANCE_STATUS, false, this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.ivNavigation:
                ivNavigation.setAlpha(1.0f);
                ivAmbulance.setAlpha(0.6f);
                stopUpdatingAmbulanceDetail();
                AndyUtils.showCustomProgressDialog(this, false);
                getAddressFromLocation(new LatLng(ambulanceDetail.getCurrentLat(), ambulanceDetail.getCurrentLng()));
                break;

            case R.id.ivLogout:
                pHelper.putDeviceToken(null);
                pHelper.putLoginType(0);
                pHelper.putSessionToken(null);
                pHelper.putUserId(0);
                pHelper.putRequestId(0);
                startActivity(new Intent(MapActivity.this, MainActivity.class));
        }
    }

    @Override
    public void onStop() {
        stopUpdatingAmbulanceDetail();
        super.onStop();
    }

    public void stopUpdatingAmbulanceDetail(){
        if(timer != null){
            timer.cancel();
            timer = null;
        }
    }

    private void getAddressFromLocation(LatLng latLng) {
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.URL, Const.GEO_CODE_API + "latlng=" + latLng.latitude + "," + latLng.longitude);
        new HttpRequester(this, map, Const.ServiceCode.GEO_CODE_API, true, this);
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        super.onTaskCompleted(response, serviceCode);
        switch (serviceCode){
            case Const.ServiceCode.CHECK_AMBULANCE_STATUS:
                Log.i("ambulanceStatus","response--->>" + response);
                AndyUtils.removeCustomProgressDialog();
                if(pContent.isSuccess(response)){
                    ambulanceDetail = pContent.parseAmbulanceStatus(response);
                    if(ambulanceDetail != null) {
                        if (isFirstCall) {
                            if (findViewById(R.id.contentFrame1) == null) {
                                HomeFragment homeFragment = new HomeFragment();
                                getSupportFragmentManager().beginTransaction().add(R.id.contentFrame, homeFragment, Const.FRAGMENT_HOME).commit();
                            } else {
                                MapFragment mapFragment = new MapFragment();
                                HomeFragment homeFragment = new HomeFragment();
                                getSupportFragmentManager().beginTransaction().add(R.id.contentFrame1, homeFragment, Const.FRAGMENT_HOME).commitAllowingStateLoss();
                                getSupportFragmentManager().beginTransaction().add(R.id.contentFrame, mapFragment, Const.FRAGMENT_MAP).commitAllowingStateLoss();
                            }
                            isFirstCall = false;
                        }
                    }
                    else{
                        if(isFirstCall){
                            AndyUtils.showToast("No request found", this);
                            isFirstCall = false;
                        }
                    }
                }
                break;

            case Const.ServiceCode.GEO_CODE_API:
                Log.i("geoCodeApi","response-->>>" + response);
                try {
                    JSONArray jsonArray = new JSONObject(response).getJSONArray(Const.KEY_RESULTS);
                    if (jsonArray.length() > 0) {
                        String strAddress = jsonArray.getJSONObject(0).getString(Const.KEY_FORMATTED_ADDRESS);
                        String uri = String.format(Locale.ENGLISH,
                                "google.navigation:q=%s", strAddress);
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                        try {
                            AndyUtils.removeCustomProgressDialog();
                            startActivity(intent);
                        } catch (ActivityNotFoundException e) {
                            Log.i("Map application",
                                    "you have to install map application");
                        }
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
                AndyUtils.removeCustomProgressDialog();
                break;

            case Const.ServiceCode.ADD_STAFF:
                if(pContent.isSuccess(response)){
                    getAmbulanceDetail();
                }
                break;
        }
    }

    public void scanQRCode(){
        Intent intent = new Intent(this,CaptureActivity.class);
        intent.setAction("com.google.zxing.client.android.SCAN");
        intent.putExtra("SAVE_HISTORY", false);
        startActivityForResult(intent, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String contents = data.getStringExtra("SCAN_RESULT");
                Log.d("TAG", "contents: " + contents);
                addStaff(contents);
            }
            else if (resultCode == RESULT_CANCELED) {
            // Handle cancel
                Log.d("TAG", "RESULT_CANCELED");
            }
        }
    }

    private void addStaff(String content){
        AndyUtils.showCustomProgressDialog(this, false);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.URL, Const.ServiceType.ADD_STAFF +
                Const.Params.REQUEST_ID + "=" + ambulanceDetail.getRequestId() + "&" +
                Const.Params.AMBULANCE_ID + "=" + ambulanceDetail.getAmbulanceId() + "&" +
                Const.Params.STAFF + "=" + content);
        new HttpRequester(this, map, Const.ServiceCode.ADD_STAFF, true, this);
    }
}

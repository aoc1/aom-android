package com.aom.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aom.R;
import com.aom.models.Staff;

import java.util.ArrayList;

public class StaffListAdapter extends BaseAdapter {
    private ArrayList<Staff> staffList;
    private LayoutInflater inflater;
    private Staff staff;

    public StaffListAdapter(Context context, ArrayList<Staff> staffList, Staff staff){
        this.staffList = staffList;
        inflater = LayoutInflater.from(context);
        this.staff = staff;
    }

    @Override
    public int getCount() {
        if(staffList != null)
            return staffList.size();
        else
            return 1;
    }

    @Override
    public Staff getItem(int position) {
        return staffList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       ViewHolder holder = new ViewHolder();
        if(convertView == null){
            convertView = inflater.inflate(R.layout.adapter_staff_list, parent, false);
            holder.tvStaffName = (TextView) convertView.findViewById(R.id.tvStaffName);
//            holder.tvDesignation = (TextView) convertView.findViewById(R.id.tvDesignation);
            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder) convertView.getTag();
        }
//        holder.tvDesignation.setText(staffList.get(position).getDesignation());
        if(staff == null) {
            holder.tvStaffName.setText(staffList.get(position).getName());
        }
        else{
            holder.tvStaffName.setText(staff.getName());
        }
        return convertView;
    }

    private class ViewHolder{
        TextView tvStaffName;
//        , tvDesignation;
    }
}

package com.aom.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aom.R;
import com.aom.models.Hospital;

import java.util.ArrayList;

public class HospitalAdapter extends BaseAdapter {
    private ArrayList<Hospital> hospitalList = new ArrayList<>();
    private LayoutInflater inflater;


    public HospitalAdapter(Context context, ArrayList<Hospital> hospitalList){
        this.hospitalList = hospitalList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return hospitalList.size();
    }

    @Override
    public Object getItem(int position) {
        return hospitalList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            convertView = inflater.inflate(R.layout.layout_hospital, parent, false);
            holder = new ViewHolder();
            holder.tvHospitalName = (TextView) convertView.findViewById(R.id.tvHospitalName);
            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tvHospitalName.setText(hospitalList.get(position).getName());
        return convertView;
    }

    private class ViewHolder{
        TextView tvHospitalName;
    }
}

package com.aom.models;


import java.io.Serializable;

public class MedicalEquipment implements Serializable{
    private int equipmentId;
    private String equipmentName, equipmentIcon;

    public String getEquipmentName() {
        return equipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }

    public String getEquipmentIcon() {
        return equipmentIcon;
    }

    public void setEquipmentIcon(String equipmentIcon) {
        this.equipmentIcon = equipmentIcon;
    }

    public int getEquipmentId() {
        return equipmentId;
    }

    public void setEquipmentId(int equipmentId) {
        this.equipmentId = equipmentId;
    }
}

package com.aom.models;

import java.io.Serializable;

/**
 * Created by mypc on 11/8/16.
 */
public class Hospital implements Serializable {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

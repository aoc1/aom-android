package com.aom.models;

import java.io.Serializable;
import java.util.ArrayList;


public class AmbulanceDetail implements Serializable{

    private String name, type, eta, isOnline, driverName, ambulanceNo, trackingTime, hospitalName, state, diseaseName, patientName, patientAge, requestNote, nextState, departureTime, fastTrack;
    private int ambulanceId, doctor, nurse, driver, er, ems, requestId, changeHospital;
    private ArrayList<String> images = new ArrayList<>();
    private ArrayList<MedicalEquipment> equipmentList = new ArrayList<>();
    private ArrayList<Staff> doctorList = new ArrayList<>(), nurseList = new ArrayList<>(), emsList = new ArrayList<>(),
        driverList = new ArrayList<>(), erList = new ArrayList<>(), staffList = new ArrayList<>();
    private ArrayList<Hospital> hospitalList = new ArrayList<>();
    private double speed;
    private double currentLat, currentLng, destLat, destLng, srcLat, srcLng;

    public String getFastTrack() {
        return fastTrack;
    }

    public int getChangeHospital() {
        return changeHospital;
    }

    public ArrayList<Hospital> getHospitalList() {
        return hospitalList;
    }

    public void setHospitalList(Hospital hospital) {
        hospitalList.add(hospital);
    }

    public void setChangeHospital(int changeHospital) {
        this.changeHospital = changeHospital;
    }

    public void setFastTrack(String fastTrack) {
        this.fastTrack = fastTrack;
    }

    public String getNextState() {
        return nextState;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public void setNextState(String nextState) {
        this.nextState = nextState;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientAge() {
        return patientAge;
    }

    public void setPatientAge(String patientAge) {
        this.patientAge = patientAge;
    }

    public String getRequestNote() {
        return requestNote;
    }

    public void setRequestNote(String requestNote) {
        this.requestNote = requestNote;
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public ArrayList<Staff> getStaffList() {
        return staffList;
    }

    public void setStaffList(ArrayList<Staff> staffList) {
        this.staffList = staffList;
    }

    public ArrayList<Staff> getDoctorList() {
        return doctorList;
    }

    public void setDoctorList(Staff doctor) {
        doctorList.add(doctor);
    }

    public ArrayList<Staff> getNurseList() {
        return nurseList;
    }

    public void setNurseList(Staff nurse) {
        nurseList.add(nurse);
    }

    public ArrayList<Staff> getEmsList() {
        return emsList;
    }

    public void setEmsList(Staff ems) {
       emsList.add(ems);
    }

    public ArrayList<Staff> getDriverList() {
        return driverList;
    }

    public void setDriverList(Staff driver) {
        driverList.add(driver);
    }

    public ArrayList<Staff> getErList() {
        return erList;
    }

    public void setErList(Staff er) {
        erList.add(er);
    }

    public String getDiseaseName() {
        return diseaseName;
    }

    public void setDiseaseName(String diseaseName) {
        this.diseaseName = diseaseName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getDoctor() {
        return doctor;
    }

    public void setDoctor(int doctor) {
        this.doctor = doctor;
    }

    public int getNurse() {
        return nurse;
    }

    public void setNurse(int nurse) {
        this.nurse = nurse;
    }

    public int getDriver() {
        return driver;
    }

    public void setDriver(int driver) {
        this.driver = driver;
    }

    public int getEr() {
        return er;
    }

    public void setEr(int er) {
        this.er = er;
    }

    public int getEms() {
        return ems;
    }

    public void setEms(int ems) {
        this.ems = ems;
    }

    public ArrayList<MedicalEquipment> getEquipmentList() {
        return equipmentList;
    }

    public void setEquipmentList(MedicalEquipment equipment) {
        this.equipmentList.add(equipment);
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getTrackingTime() {
        return trackingTime;
    }

    public void setTrackingTime(String trackingTime) {
        this.trackingTime = trackingTime;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public double getSrcLat() {
        return srcLat;
    }

    public void setSrcLat(double srcLat) {
        this.srcLat = srcLat;
    }

    public double getSrcLng() {
        return srcLng;
    }

    public void setSrcLng(double srcLng) {
        this.srcLng = srcLng;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(String image) {
        images.add(image);
    }

    public String getAmbulanceNo() {
        return ambulanceNo;
    }

    public void setAmbulanceNo(String ambulanceNo) {
        this.ambulanceNo = ambulanceNo;
    }

    public double getCurrentLat() {
        return currentLat;
    }

    public void setCurrentLat(double currentLat) {
        this.currentLat = currentLat;
    }

    public double getCurrentLng() {
        return currentLng;
    }

    public void setCurrentLng(double currentLng) {
        this.currentLng = currentLng;
    }

    public double getDestLat() {
        return destLat;
    }

    public void setDestLat(double destLat) {
        this.destLat = destLat;
    }

    public double getDestLng() {
        return destLng;
    }

    public void setDestLng(double destLng) {
        this.destLng = destLng;
    }

    public String getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(String isOnline) {
        this.isOnline = isOnline;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public int getAmbulanceId() {
        return ambulanceId;
    }

    public void setAmbulanceId(int ambulanceId) {
        this.ambulanceId = ambulanceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEta() {
        return eta;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }
}

package com.aom.parse;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;

import com.aom.models.AmbulanceDetail;
import com.aom.models.Hospital;
import com.aom.models.MedicalEquipment;
import com.aom.models.Staff;
import com.aom.utils.Const;
import com.aom.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;

public class ParseContent {

    private Activity activity;

    public ParseContent(Activity activity) {
        this.activity = activity;
    }

    public boolean isSuccess(String response) {
        final String IS_SUCCESS = "success";
        if (TextUtils.isEmpty(response))
            return false;

        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getBoolean(IS_SUCCESS))
                return true;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void parseUser(String response) {
        PreferenceHelper preferenceHelper = new PreferenceHelper(activity);
        String QR_CODE = "qrcode";
        try {
            JSONObject jsonObject = new JSONObject(response);
            preferenceHelper.putUserId(jsonObject.getInt(Const.Params.ID));
            preferenceHelper.putUserEmail(jsonObject.getString(Const.Params.EMAIL));
            preferenceHelper.putSessionToken(jsonObject.getString(Const.Params.TOKEN));
            preferenceHelper.putQRCodeLogin(jsonObject.getInt(QR_CODE));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public AmbulanceDetail parseAmbulanceStatus(String response) {
        AmbulanceDetail ambDetail = null;

        final String NUMBER_LIST = "number_list" ,NUMBER = "number", EMS_STAFF = "ems_staff", ER_STAFF = "er_staff", STATE = "state",
                DOCTOR = "doctor", NURSE = "nurse", DRIVER = "driver", ER = "er", EMS = "ems", DISEASE_NAME = "patient_sick",
                PATIENT_NAME = "patient_name", PATIENT_AGE = "patient_age", REQUEST_NOTE = "request_note", REQUEST_ID = "request_id", NEXT_STATE = "next_state", CHANGE_HOSPITAL = "change_hospital",
                AMBULANCE_ID = "ambulance_id", AMBULANCE_TYPE = "ambulance_type", TRACKING_SPEED = "tracking_speed", CURRENT_LAT = "tracking_latitude", CURRENT_LNG = "tracking_longitude",
                AMBULANCE_NO = "ambulance_no", DEST_LAT = "ambulance_to_latitude", DEST_LNG = "ambulance_to_longitude", AMBULANCE_NAME = "ambulance_name", DRIVER_NAME = "driver_name",
                SRC_LAT = "ambulance_from_latitude", SRC_LNG = "ambulance_from_longitude", TRACKING_TIME = "tracking_add_time", MEDICAL_EQUIPMENT = "medical_equipment", ICON_IMAGE = "icon_image",
                NAME = "name", REQUEST = "req", SOURCE_HOSPITAL = "source_hospital", DEST_HOSPITAL = "dest_hospital", FAST_TRACK = "fast_track", DEPARTURE_TIME = "request_add_time", HOSPITAL_LIST = "hospital_list";

        try {
            JSONObject jsonObject = new JSONObject(response);
//            Object request = new JSONTokener(REQUEST).nextValue();
            if (jsonObject.has(REQUEST) && jsonObject.getJSONObject(REQUEST) != null) {
                JSONObject jObject = jsonObject.getJSONObject(REQUEST);
                ambDetail = new AmbulanceDetail();
                ambDetail.setSpeed(Double.parseDouble(jObject.getString(TRACKING_SPEED)));
                ambDetail.setTrackingTime(jObject.getString(TRACKING_TIME));
                ambDetail.setState(jObject.getString(STATE));
                ambDetail.setNextState(jObject.getString(NEXT_STATE));
                ambDetail.setAmbulanceId(jObject.getInt(AMBULANCE_ID));
                ambDetail.setType(jObject.getString(AMBULANCE_TYPE));
                ambDetail.setCurrentLat(Double.parseDouble(jObject.getString(CURRENT_LAT)));
                ambDetail.setCurrentLng(Double.parseDouble(jObject.getString(CURRENT_LNG)));
                ambDetail.setDestLat(Double.parseDouble(jObject.getString(DEST_LAT)));
                ambDetail.setDestLng(Double.parseDouble(jObject.getString(DEST_LNG)));
                ambDetail.setAmbulanceNo(URLDecoder.decode(jObject.getString(AMBULANCE_NO), "utf-8"));
                ambDetail.setName(URLDecoder.decode(jObject.getString(AMBULANCE_NAME), "utf-8"));
                ambDetail.setDriverName(URLDecoder.decode(jObject.getString(DRIVER_NAME), "utf-8"));
                ambDetail.setDoctor(jObject.getInt(DOCTOR));
                ambDetail.setDriver(jObject.getInt(DRIVER));
                ambDetail.setNurse(jObject.getInt(NURSE));
                ambDetail.setEr(jObject.getInt(ER));
                ambDetail.setEms(jObject.getInt(EMS));
                ambDetail.setDiseaseName(jObject.getString(DISEASE_NAME));
                ambDetail.setPatientAge(jObject.getString(PATIENT_AGE));
                ambDetail.setPatientName(jObject.getString(PATIENT_NAME));
                ambDetail.setRequestId(jObject.getInt(REQUEST_ID));
                ambDetail.setRequestNote(jObject.getString(REQUEST_NOTE));
                ambDetail.setFastTrack(jObject.getString(FAST_TRACK));
                ambDetail.setChangeHospital(jObject.getInt(CHANGE_HOSPITAL));
                ambDetail.setDepartureTime(jObject.getString(DEPARTURE_TIME).substring(jObject.getString(DEPARTURE_TIME).indexOf(' ')));

                JSONArray hospitalArray = jObject.getJSONArray(HOSPITAL_LIST);
                if(hospitalArray.length() > 0){
                    for(int i=0; i<hospitalArray.length(); i++){
                        Hospital hospital = new Hospital();
                        JSONObject object1 = hospitalArray.getJSONObject(i);
                        hospital.setId(object1.getInt(Const.Params.ID));
                        hospital.setName(object1.getString(NAME));
                        ambDetail.setHospitalList(hospital);
                    }
                }

                JSONArray equipmentArray = jObject.getJSONArray(MEDICAL_EQUIPMENT);
                if (equipmentArray.length() > 0) {
                    for (int j = 0; j < equipmentArray.length(); j++) {
                        JSONObject object = equipmentArray.getJSONObject(j);
                        MedicalEquipment equipment = new MedicalEquipment();
                        equipment.setEquipmentId(object.getInt(Const.Params.ID));
                        equipment.setEquipmentName(object.getString(NAME));
                        equipment.setEquipmentIcon(object.getString(ICON_IMAGE));
                        ambDetail.setEquipmentList(equipment);
                    }
                }

                if (jObject.has(NUMBER_LIST)) {
                    JSONArray numberArray = jObject.getJSONArray(NUMBER_LIST);
                    if (numberArray.length() > 0) {
                        ArrayList<Staff> staffList = new ArrayList<>();
                        for (int j = 0; j < numberArray.length(); j++) {
                            JSONObject object = numberArray.getJSONObject(j);
                            if (object.has(DOCTOR)) {
                                JSONArray staffArray = object.getJSONArray(DOCTOR);
                                if (staffArray.length() > 0) {
                                    for (int k = 0; k < staffArray.length(); k++) {
                                        JSONObject jsonObject1 = staffArray.getJSONObject(k);
                                        Staff staff = new Staff();
                                        staff.setDesignation(DOCTOR);
                                        staff.setName(jsonObject1.getString(NAME));
                                        if (!TextUtils.isEmpty(jsonObject1.getString(NUMBER))) {
                                            staff.setNumber(jsonObject1.getString(NUMBER));
//                                            staffList.add(staff);
                                        }
                                        ambDetail.setDoctorList(staff);
                                    }
                                }
                            }

                            if (object.has(NURSE)) {
                                JSONArray staffArray = object.getJSONArray(NURSE);
                                if (staffArray.length() > 0) {
                                    for (int k = 0; k < staffArray.length(); k++) {
                                        JSONObject jsonObject1 = staffArray.getJSONObject(k);
                                        Staff staff = new Staff();
                                        staff.setDesignation(NURSE);
                                        staff.setName(jsonObject1.getString(NAME));
                                        if (!TextUtils.isEmpty(jsonObject1.getString(NUMBER))) {
                                            staff.setNumber(jsonObject1.getString(NUMBER));
//                                            staffList.add(staff);
                                        }
                                        ambDetail.setNurseList(staff);
                                    }
                                }
                            }

                            if (object.has(DRIVER)) {
                                Log.i("parseContent","driver--->> if called");
                                JSONArray staffArray = object.getJSONArray(DRIVER);
                                if (staffArray.length() > 0) {
                                    for (int k = 0; k < staffArray.length(); k++) {
                                        JSONObject jsonObject1 = staffArray.getJSONObject(k);
                                        Staff staff = new Staff();
                                        staff.setDesignation(DRIVER);
                                        staff.setName(jsonObject1.getString(NAME));
                                        if (!TextUtils.isEmpty(jsonObject1.getString(NUMBER))) {
                                            staff.setNumber(jsonObject1.getString(NUMBER));
//                                            staffList.add(staff);
                                        }
                                        ambDetail.setDriverList(staff);
                                    }
                                }
                            }

                            if (object.has(EMS_STAFF)) {
                                JSONArray staffArray = object.getJSONArray(EMS_STAFF);
                                if (staffArray.length() > 0) {
                                    for (int k = 0; k < staffArray.length(); k++) {
                                        JSONObject jsonObject1 = staffArray.getJSONObject(k);
                                        Staff staff = new Staff();
                                        staff.setDesignation(EMS_STAFF);
                                        staff.setName(jsonObject1.getString(NAME));
                                        if (!TextUtils.isEmpty(jsonObject1.getString(NUMBER))) {
                                            staff.setNumber(jsonObject1.getString(NUMBER));
//                                            staffList.add(staff);
                                        }
                                        ambDetail.setEmsList(staff);
                                    }
                                }
                            }

                            if (object.has(ER_STAFF)) {
                                JSONArray staffArray = object.getJSONArray(ER_STAFF);
                                if (staffArray.length() > 0) {
                                    for (int k = 0; k < staffArray.length(); k++) {
                                        JSONObject jsonObject1 = staffArray.getJSONObject(k);
                                        Staff staff = new Staff();
                                        staff.setDesignation(ER_STAFF);
                                        staff.setName(jsonObject1.getString(NAME));
                                        if (!TextUtils.isEmpty(jsonObject1.getString(NUMBER))) {
                                            staff.setNumber(jsonObject1.getString(NUMBER));
//                                            staffList.add(staff);
                                        }
                                        ambDetail.setErList(staff);
                                    }
                                }
                            }

                            if(object.has(SOURCE_HOSPITAL)){
                                JSONArray staffArray = object.getJSONArray(SOURCE_HOSPITAL);
                                if (staffArray.length() > 0) {
                                    for (int k = 0; k < staffArray.length(); k++) {
                                        JSONObject jsonObject1 = staffArray.getJSONObject(k);
                                        if (!TextUtils.isEmpty(jsonObject1.getString(NUMBER))) {
                                            Staff staff = new Staff();
                                            staff.setName(jsonObject1.getString(NAME));
                                            staff.setNumber(jsonObject1.getString(NUMBER));
                                            staffList.add(staff);
                                        }
                                    }
                                }
                            }

                            if(object.has(DEST_HOSPITAL)){
                                JSONArray staffArray = object.getJSONArray(DEST_HOSPITAL);
                                if (staffArray.length() > 0) {
                                    for (int k = 0; k < staffArray.length(); k++) {
                                        JSONObject jsonObject1 = staffArray.getJSONObject(k);
                                        if (!TextUtils.isEmpty(jsonObject1.getString(NUMBER))) {
                                            Staff staff = new Staff();
                                            staff.setName(jsonObject1.getString(NAME));
                                            staff.setNumber(jsonObject1.getString(NUMBER));
                                            staffList.add(staff);
                                        }
                                    }
                                }
                            }
                        }

                        ambDetail.setStaffList(staffList);
                    }
                }
                ambDetail.setSrcLat(Double.parseDouble(jObject.getString(SRC_LAT)));
                ambDetail.setSrcLng(Double.parseDouble(jObject.getString(SRC_LNG)));
            }
        }

        catch (JSONException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return ambDetail;
    }
}
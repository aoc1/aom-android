package com.aom;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;

import com.aom.location.LocationHelper;
import com.aom.models.AmbulanceDetail;
import com.aom.parse.HttpRequester;
import com.aom.parse.ParseContent;
import com.aom.utils.AndyUtils;
import com.aom.utils.Const;
import com.aom.utils.PreferenceHelper;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;

public class MainActivity extends BaseActivity implements LocationHelper.OnLocationReceived{

    private EditText etEmail, etPassword;
    private ParseContent pContent;
    private Location myLocation;
    private PreferenceHelper pHelper;

    private String strAddress = "";

    private LocationHelper locationHelper;
    private int type;
    private AmbulanceDetail ambDetail = new AmbulanceDetail();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Spinner spCategory;
        super.onCreate(savedInstanceState);
        if(getIntent() != null && getIntent().getBooleanExtra("Exit me", false)){
            this.finish();
            return;
        }
        if(new PreferenceHelper(this).getUserId() != 0){
            startActivity(new Intent(this, MapActivity.class));
            finish();
        }

        setContentView(R.layout.activity_login);
        pContent = new ParseContent(this);
        pHelper = new PreferenceHelper(this);

        if(findViewById(R.id.llLoginTab) == null) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        else{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etEPassword);
        spCategory = (Spinner) findViewById(R.id.spCategory);
        assert spCategory != null;
        spCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        type = 7;
                        break;

                    case 1:
                        type = 1;
                        break;

                    case 2:
                        type = 2;
                        break;

                    case 3:
                        type = 3;
                        break;

                    case 4:
                        type = 4;
                        break;

                    case 5:
                        type = 6;
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        findViewById(R.id.tvForgetPwd).setOnClickListener(this);
        findViewById(R.id.btnLogin).setOnClickListener(this);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
        else{
            locationHelper = new LocationHelper(this);
            locationHelper.setLocationReceivedLister(this);
            locationHelper.onStart();
        }
    }

    private void checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.i("Helper","startPeriodicUpdate if called");
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(this, new String[] {android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, Const.PERMISSION_LOCATION);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnLogin:
                if(isValidate()) {
                    if (myLocation != null) {
                        new PreferenceHelper(this).putLoginType(type);
                        login();
                    }
                    else if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                        checkLocationPermission();
                    }
                }
                break;

            case R.id.tvForgetPwd:
                break;
        }
    }

    private void login(){
        AndyUtils.showCustomProgressDialog(this, false);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.URL, Const.ServiceType.LOGIN);
        map.put(Const.Params.EMAIL, etEmail.getText().toString());
        map.put(Const.Params.PASSWORD, etPassword.getText().toString());
        map.put(Const.Params.DEVICE_TYPE, Const.DEVICE_TYPE);
        map.put(Const.Params.DEVICE_TOKEN, pHelper.getDeviceToken());
        map.put(Const.Params.LATITUDE, String.valueOf(myLocation.getLatitude()));
        map.put(Const.Params.LONGITUDE, String.valueOf(myLocation.getLongitude()));
        map.put(Const.Params.LOCATION, strAddress);
        map.put(Const.Params.TYPE, String.valueOf(type));
        new HttpRequester(this, map, Const.ServiceCode.LOGIN, false, this);
    }

//    private void login(){
//        AndyUtils.showCustomProgressDialog(this, false);
//        HashMap<String, String> map = new HashMap<>();
//        map.put(Const.URL, Const.ServiceType.LOGIN);
//        map.put(Const.Params.EMAIL, "aocuser@gmail.com");
//        map.put(Const.Params.PASSWORD, "1234");
//        map.put(Const.Params.DEVICE_TYPE, "android");
//        map.put(Const.Params.DEVICE_TOKEN, String.valueOf(12121212));
//        map.put(Const.Params.LATITUDE, "1.000");
//        map.put(Const.Params.LONGITUDE, "2.000");
//        map.put(Const.Params.LOCATION, "2 Infinite Loop, Cupertino, CA 95014, USA");
//        map.put(Const.Params.TYPE, String.valueOf(7));
//        new HttpRequester(this, map, Const.ServiceCode.LOGIN, false, this);
//    }

    private boolean isValidate(){
        String msg = null;
        if(TextUtils.isEmpty(etEmail.getText()))
            msg = getString(R.string.text_error_empty_email);

        else if(TextUtils.isEmpty(etPassword.getText()))
            msg = getString(R.string.text_error_empty_password);

        else if(type == 0){
            msg = getString(R.string.text_error_empty_type);
        }

        if(msg == null)
            return true;
        else {
            AndyUtils.showToast(msg, this);
            return false;
        }
    }

    @Override
    public void onLocationReceived(LatLng latLng) {
    }

    @Override
    public void onLocationReceived(Location location) {
        Log.i("locReceived","location-->>>" + location);
        if (location != null) {
            myLocation = location;
        }
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnected(Location location) {
        Log.i("onConnected","location-->>>" + location);
        if(location != null){
            myLocation = location;
            getAddressFromLocation(new LatLng(location.getLatitude(), location.getLongitude()));
        }
        else{
            showLocationOffDialog();
        }
    }

    private void getAddressFromLocation(LatLng latLng) {
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.URL, Const.GEO_CODE_API + "latlng=" + latLng.latitude + "," + latLng.longitude);
        new HttpRequester(this, map, Const.ServiceCode.GEO_CODE_API, true, this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case Const.PERMISSION_LOCATION:
                if(grantResults.length > 0){
                    if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                        AndyUtils.showCustomProgressDialog(this, false);
                        locationHelper = new LocationHelper(this);
                        locationHelper.setLocationReceivedLister(this);
                        locationHelper.onStart();
                    }
                }
                break;
        }
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        switch (serviceCode){
            case Const.ServiceCode.LOGIN:
                AndyUtils.removeCustomProgressDialog();
                Log.i("LoginResponse", response);
                if(pContent.isSuccess(response)){
                    pContent.parseUser(response);
                    Intent intent = new Intent(MainActivity.this, MapActivity.class);
//                    intent.putExtra("ambDetail", ambDetail);
                    startActivity(intent);
                    MainActivity.this.finish();
                }
                else{
                    AndyUtils.showToast("Error", this);
                }
                break;

            case Const.ServiceCode.GEO_CODE_API:
                Log.i("geoCodeApi","response-->>>" + response);
                try {
                    JSONArray jsonArray = new JSONObject(response).getJSONArray(Const.KEY_RESULTS);
                    if (jsonArray.length() > 0) {
                        strAddress = jsonArray.getJSONObject(0).getString(Const.KEY_FORMATTED_ADDRESS);
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
                AndyUtils.removeCustomProgressDialog();
                break;
        }
    }
}

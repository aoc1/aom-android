package com.aom.fragments;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.aom.R;
import com.aom.adapter.HospitalAdapter;
import com.aom.adapter.StaffListAdapter;
import com.aom.models.Staff;
import com.aom.parse.HttpRequester;
import com.aom.utils.AndyUtils;
import com.aom.utils.Const;
import com.google.zxing.client.android.CaptureActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class HomeFragment extends BaseFragment implements View.OnClickListener{

    private TextView tvAmbName, tvAmbNo, tvAmbType, tvAmbSpeed, tvDepartureTime, tvArrivalTime, tvPatientName, tvPatientAge, tvDoctorNote, tvDiseaseType, tvStatus, tvHospital, tvFastTrack;
    private Button btnChangeStatus, btnScanCode;
    private LinearLayout llEquipment, llEquipmentIcon, llStaff, llStaffIcon;
    private Timer timer;
    private boolean isFirstCall = true;
    private Staff staff;
    private SeekBar pBarAmb;
    StaffListAdapter staffListAdapter;
    private double dist;
    private Spinner spHospital;
    public boolean sendActionParam = false;
    public int hospitalId;
    private String  to = "",from = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        tvAmbName = (TextView) view.findViewById(R.id.tvAmbName);
        tvAmbNo = (TextView) view.findViewById(R.id.tvAmbNo);
        pBarAmb = (SeekBar) view.findViewById(R.id.pBarAmb);
        tvAmbType = (TextView) view.findViewById(R.id.tvAmbType);
        tvAmbSpeed = (TextView) view.findViewById(R.id.tvAmbSpeed);
        tvDepartureTime = (TextView) view.findViewById(R.id.tvDepartureTime);
        tvArrivalTime = (TextView) view.findViewById(R.id.tvArrivalTime);
        tvPatientName = (TextView) view.findViewById(R.id.tvPatientName);
        tvPatientAge = (TextView) view.findViewById(R.id.tvPatientAge);
        tvDoctorNote = (TextView) view.findViewById(R.id.tvDoctorNote);
        tvDiseaseType = (TextView) view.findViewById(R.id.tvDiseaseType);
        tvStatus = (TextView) view.findViewById(R.id.tvStatus);
        tvHospital = (TextView) view.findViewById(R.id.tvHospital);
        tvFastTrack = (TextView) view.findViewById(R.id.tvFastTrack);
        view.findViewById(R.id.ivCall).setOnClickListener(this);
        btnChangeStatus = (Button) view.findViewById(R.id.btnChangeStatus);
        btnScanCode = (Button) view.findViewById(R.id.btnScanCode);
        if(pHelper.getQRCodeLogin() == 1){
            btnScanCode.setVisibility(View.VISIBLE);
        }
        btnScanCode.setOnClickListener(this);
        btnChangeStatus.setOnClickListener(this);
        llEquipment = (LinearLayout) view.findViewById(R.id.llEquipment);
        llEquipmentIcon = (LinearLayout) view.findViewById(R.id.llEquipmentIcon);
        llStaff = (LinearLayout) view.findViewById(R.id.llStaff);
        llStaffIcon = (LinearLayout) view.findViewById(R.id.llStaffIcon);
        spHospital = (Spinner) view.findViewById(R.id.spHospital);
        spHospital.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                to = activity.getString(R.string.text_to) + " " + activity.ambulanceDetail.getHospitalList().get(position).getName();
                tvHospital.setText(from + " " + to);
                hospitalId = activity.ambulanceDetail.getHospitalList().get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        AndyUtils.showCustomProgressDialog(activity, false);
        pBarAmb.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        pBarAmb.setThumb(new ColorDrawable(ContextCompat.getColor(activity, R.color.color_green_button)));
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        startUpdatingDetails();
    }

    private void setData(){
        if(activity.ambulanceDetail != null) {
            LayoutInflater inflater;
            View view = null;
            boolean isFirstIteration = true;
            ImageView ivStaffIcon;
            TextView tvStaffName = null;

            String ETA;

            if(isFirstCall) {
                tvAmbName.setText(activity.ambulanceDetail.getName());
                tvAmbNo.setText(activity.ambulanceDetail.getAmbulanceNo());
                tvAmbType.setText(activity.ambulanceDetail.getType());
                tvDiseaseType.setText(activity.ambulanceDetail.getDiseaseName());
                if(!TextUtils.isEmpty(activity.ambulanceDetail.getFastTrack())) {
                    tvFastTrack.setText(activity.ambulanceDetail.getFastTrack());
                    tvFastTrack.setVisibility(View.VISIBLE);
                }
                from = activity.getString(R.string.text_from) + " " + activity.ambulanceDetail.getStaffList().get(0).getName();

                if(activity.ambulanceDetail.getType().contains(Const.TYPE_REF)) {
                    to = activity.getString(R.string.text_to) + " " + activity.ambulanceDetail.getStaffList().get(1).getName();
                    tvHospital.setText(from + to);
                }

                if(!TextUtils.isEmpty(activity.ambulanceDetail.getPatientAge())) {
                    tvPatientAge.setText(getString(R.string.text_patient_age) + " " + activity.ambulanceDetail.getPatientAge());
                    tvPatientAge.setVisibility(View.VISIBLE);
                }

                if(!TextUtils.isEmpty(activity.ambulanceDetail.getPatientName())) {
                    tvPatientName.setText(getString(R.string.text_patient_name) + " " + activity.ambulanceDetail.getPatientName());
                    tvPatientName.setVisibility(View.VISIBLE);
                }

                if(!TextUtils.isEmpty(activity.ambulanceDetail.getRequestNote())) {
                    tvDoctorNote.setText(activity.ambulanceDetail.getRequestNote());
                    tvDoctorNote.setVisibility(View.VISIBLE);
                }

                if (activity.ambulanceDetail.getEquipmentList().size() > 0) {
                    llEquipment.setVisibility(View.VISIBLE);
                    for (int i = 0; i < activity.ambulanceDetail.getEquipmentList().size(); i++) {
                        inflater = LayoutInflater.from(activity);
                        view = inflater.inflate(R.layout.layout_icon_inflater, null);
                        ImageView ivIcon = (ImageView) view.findViewById(R.id.ivIcon);
                        new AQuery(activity).id(ivIcon).image(activity.ambulanceDetail.getEquipmentList().get(i).getEquipmentIcon());
                        if (ivIcon.getParent() != null) {
                            ((ViewGroup) ivIcon.getParent()).removeView(ivIcon);
                        }
                        llEquipmentIcon.addView(ivIcon);
                    }
                }
                else{
                    llEquipment.setVisibility(View.GONE);
                }

                isFirstCall = false;
            }

            if(llStaffIcon.getChildCount() > 0){
                Log.i("setData","if called");
                Log.i("setData","Child count -->> " + llStaffIcon.getChildCount());
//                for(int i=0; i<llStaffIcon.getChildCount(); i++){
//                    llStaffIcon.removeViewAt(i);
//                }
                llStaffIcon.removeAllViews();
            }

            if(activity.ambulanceDetail.getDoctorList() != null && activity.ambulanceDetail.getDoctorList().size() > 0){
                inflater = LayoutInflater.from(activity);

                for(int i=0; i<activity.ambulanceDetail.getDoctorList().size(); i++){
                    Staff staff = activity.ambulanceDetail.getDoctorList().get(i);
                    if(isFirstIteration) {
                        view = inflater.inflate(R.layout.adapter_staff, null, false);
                        ivStaffIcon = (ImageView) view.findViewById(R.id.ivStaffIcon);
                        tvStaffName = (TextView) view.findViewById(R.id.tvStaffName);
                        ivStaffIcon.setImageResource(R.mipmap.doctor);
                        tvStaffName.setText(staff.getName());
                        isFirstIteration = false;
                    }
                    else{
                        tvStaffName.append("," + staff.getName());
                    }
                }

                assert view != null;
                if(view.getParent() != null){
                    ((ViewGroup)view.getParent()).removeView(view);
                }
                llStaffIcon.addView(view);
                isFirstIteration = true;
            }

            if(activity.ambulanceDetail.getDriverList() != null && activity.ambulanceDetail.getDriverList().size() > 0){
                Log.i("setData","DriverList-->>> if called");
                inflater = LayoutInflater.from(activity);
                for(int i=0; i<activity.ambulanceDetail.getDriverList().size(); i++){
                    Staff staff = activity.ambulanceDetail.getDriverList().get(i);
                    if(isFirstIteration) {
                        view = inflater.inflate(R.layout.adapter_staff, null, false);
                        ivStaffIcon = (ImageView) view.findViewById(R.id.ivStaffIcon);
                        tvStaffName = (TextView) view.findViewById(R.id.tvStaffName);
                        ivStaffIcon.setImageResource(R.mipmap.driver);
                        tvStaffName.setText(staff.getName());
                        isFirstIteration = false;
                    }
                    else{
                        tvStaffName.append("," + staff.getName());
                    }
                }

                assert view != null;
                if(view.getParent() != null){
                    ((ViewGroup)view.getParent()).removeView(view);
                }
                llStaffIcon.addView(view);
                isFirstIteration = true;
            }

            if(activity.ambulanceDetail.getNurseList() != null && activity.ambulanceDetail.getNurseList().size() > 0){
                inflater = LayoutInflater.from(activity);
                for(int i=0; i<activity.ambulanceDetail.getNurseList().size(); i++){
                    Staff staff = activity.ambulanceDetail.getNurseList().get(i);
                    if(isFirstIteration) {
                        view = inflater.inflate(R.layout.adapter_staff, null, false);
                        ivStaffIcon = (ImageView) view.findViewById(R.id.ivStaffIcon);
                        tvStaffName = (TextView) view.findViewById(R.id.tvStaffName);
                        ivStaffIcon.setImageResource(R.mipmap.nurse);
                        tvStaffName.setText(staff.getName());
                        isFirstIteration = false;
                    }
                    else{
                        tvStaffName.append("," + staff.getName());
                    }
                }

                assert view != null;
                if(view.getParent() != null){
                    ((ViewGroup)view.getParent()).removeView(view);
                }
                llStaffIcon.addView(view);
                isFirstIteration = true;
            }

            if(activity.ambulanceDetail.getEmsList() != null && activity.ambulanceDetail.getEmsList().size() > 0){
                inflater = LayoutInflater.from(activity);
                for(int i=0; i<activity.ambulanceDetail.getEmsList().size(); i++){
                    Staff staff = activity.ambulanceDetail.getEmsList().get(i);
                    if(isFirstIteration) {
                        view = inflater.inflate(R.layout.adapter_staff, null, false);
                        ivStaffIcon = (ImageView) view.findViewById(R.id.ivStaffIcon);
                        tvStaffName = (TextView) view.findViewById(R.id.tvStaffName);
                        ivStaffIcon.setImageResource(R.mipmap.emr);
                        tvStaffName.setText(staff.getName());
                        isFirstIteration = false;
                    }
                    else{
                        tvStaffName.append("," + staff.getName());
                    }
                }

                assert view != null;
                if(view.getParent() != null){
                    ((ViewGroup)view.getParent()).removeView(view);
                }
                llStaffIcon.addView(view);
                isFirstIteration = true;
            }

            if(activity.ambulanceDetail.getErList() != null && activity.ambulanceDetail.getErList().size() > 0){
                inflater = LayoutInflater.from(activity);
                for(int i=0; i<activity.ambulanceDetail.getErList().size(); i++){
                    Staff staff = activity.ambulanceDetail.getErList().get(i);
                    if(isFirstIteration) {
                        view = inflater.inflate(R.layout.adapter_staff, null, false);
                        ivStaffIcon = (ImageView) view.findViewById(R.id.ivStaffIcon);
                        tvStaffName = (TextView) view.findViewById(R.id.tvStaffName);
                        ivStaffIcon.setImageResource(R.mipmap.ref_patient);
                        tvStaffName.setText(staff.getName());
                        isFirstIteration = false;
                    }
                    else{
                        tvStaffName.append("," + staff.getName());
                    }
                }

                assert view != null;
                if(view.getParent() != null){
                    ((ViewGroup)view.getParent()).removeView(view);
                }
                llStaffIcon.addView(view);
            }

            if(llStaffIcon.getChildCount() > 0){
                llStaff.setVisibility(View.VISIBLE);
            }

            Location location1 = new Location("");
            Location location2 = new Location("");
            location1.setLatitude(activity.ambulanceDetail.getSrcLat());
            location1.setLongitude(activity.ambulanceDetail.getSrcLng());
            location2.setLatitude(activity.ambulanceDetail.getDestLat());
            location2.setLongitude(activity.ambulanceDetail.getDestLng());

            double totalDist = location1.distanceTo(location2);

            location1 = new Location("");
            location2 = new Location("");
            location1.setLatitude(activity.ambulanceDetail.getSrcLat());
            location1.setLongitude(activity.ambulanceDetail.getSrcLng());
            location2.setLatitude(activity.ambulanceDetail.getCurrentLat());
            location2.setLongitude(activity.ambulanceDetail.getCurrentLng());

            double coveredDist = location1.distanceTo(location2);
            pBarAmb.setProgress((int) Math.floor(Math.floor((100 * coveredDist) / totalDist)));
            AndyUtils.removeCustomProgressDialog();

            btnChangeStatus.setText(activity.ambulanceDetail.getNextState());
            tvStatus.setText(activity.ambulanceDetail.getState());
            dist = Math.sqrt(Math.pow(activity.ambulanceDetail.getCurrentLat() - activity.ambulanceDetail.getDestLat(), 2) + Math.pow(activity.ambulanceDetail.getCurrentLng() - activity.ambulanceDetail.getDestLng(), 2));
            ETA = String.format(Locale.ENGLISH, "%.2f", (dist * 100) / (activity.ambulanceDetail.getSpeed() * 60 / 1000)) + " Min " + String.format(Locale.ENGLISH, "%.2f", activity.ambulanceDetail.getSpeed() * 60 * 60 / 1000) + " km/h ";
            tvAmbSpeed.setText(ETA);
            if ((activity.ambulanceDetail.getSpeed() * 60 * 60 / 1000) > 80 && (activity.ambulanceDetail.getSpeed() * 60 * 60 / 1000) < 120) {
                tvAmbSpeed.setTextColor(ContextCompat.getColor(activity, R.color.color_orange));
            } else if ((activity.ambulanceDetail.getSpeed() * 60 * 60 / 1000) >= 120) {
                tvAmbSpeed.setTextColor(ContextCompat.getColor(activity, R.color.color_red));
            } else {
                tvAmbSpeed.setTextColor(ContextCompat.getColor(activity, R.color.color_green));
            }
            tvDepartureTime.setText(activity.ambulanceDetail.getDepartureTime());
            tvArrivalTime.setText(calculateArrivalTime());

            if(activity.ambulanceDetail.getType().equalsIgnoreCase(Const.TYPE_EMS) && activity.ambulanceDetail.getChangeHospital() == 0 && spHospital.getVisibility() == View.VISIBLE){
                spHospital.setVisibility(View.GONE);
                sendActionParam = false;
            }
        }
        else{
            if(isFirstCall) {
                AndyUtils.showToast("No request found", activity);
                isFirstCall = false;
            }
        }
        AndyUtils.removeCustomProgressDialog();
    }

    private String calculateArrivalTime(){
        SimpleDateFormat sdf = new SimpleDateFormat("kk:mm:ss", Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, (int)((dist * 100) / (activity.ambulanceDetail.getSpeed() * 60 / 1000)));
        return sdf.format(calendar.getTime());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnChangeStatus:
                if(activity.ambulanceDetail.getChangeHospital() == 1 && activity.ambulanceDetail.getType().equalsIgnoreCase(Const.TYPE_EMS)){
                    HospitalAdapter hospitalAdapter = new HospitalAdapter(activity, activity.ambulanceDetail.getHospitalList());
                    spHospital.setAdapter(hospitalAdapter);
                    spHospital.setVisibility(View.VISIBLE);
                    sendActionParam = true;
                }
                else if(spHospital.getVisibility() == View.VISIBLE){
                    spHospital.setVisibility(View.GONE);
                    sendActionParam = false;
                }
                updateStatus();
                break;

            case R.id.btnScanCode:
                activity.scanQRCode();
                break;

            case R.id.ivCall:
                showStaffListDialog();
                break;
        }
    }

    private void showStaffListDialog() {
        stopUpdatingDetails();

        Dialog staffListDialog = new Dialog(activity);
        staffListDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        staffListDialog.setContentView(R.layout.dialog_staff_list);
        ListView lvStaffList = (ListView) staffListDialog.findViewById(R.id.lvStaffList);
        if(activity.ambulanceDetail.getType().equalsIgnoreCase(Const.TYPE_EMS)){
            staffListAdapter = new StaffListAdapter(activity, null, activity.ambulanceDetail.getStaffList().get(0));
        }
        else if(activity.ambulanceDetail.getType().contains(Const.TYPE_REF)){
            staffListAdapter = new StaffListAdapter(activity, activity.ambulanceDetail.getStaffList(), null);
        }

        lvStaffList.setAdapter(staffListAdapter);
        lvStaffList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                staff = staffListAdapter.getItem(position);
                makeACall();
            }
        });
        staffListDialog.show();
    }

    public void makeACall(){
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + staff.getNumber()));
        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
//                    return;
                requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, Const.PERMISSION_CALL);
//                return;
            }
            else{
                activity.startActivity(intent);
            }
        }
        else{
            activity.startActivity(intent);
        }
    }

    public void updateStatus(){
        activity.stopUpdatingAmbulanceDetail();
        AndyUtils.showCustomProgressDialog(activity, false);
        HashMap<String, String> map = new HashMap<>();
        if(!sendActionParam) {
            map.put(Const.URL, Const.ServiceType.UPDATE_STATUS +
                    Const.Params.REQUEST_ID + "=" + String.valueOf(activity.ambulanceDetail.getRequestId()) +
                    Const.Params.AMBULANCE_ID + "=" + String.valueOf(activity.ambulanceDetail.getAmbulanceId()));
        }
        else {
            map.put(Const.URL, Const.ServiceType.UPDATE_STATUS +
                    Const.Params.REQUEST_ID + "=" + String.valueOf(activity.ambulanceDetail.getRequestId()) +
                    Const.Params.AMBULANCE_ID + "=" + String.valueOf(activity.ambulanceDetail.getAmbulanceId()) +
                    Const.Params.HOSPITAL_ID + "=" + String.valueOf(hospitalId));
        }
        new HttpRequester(activity, map, Const.ServiceCode.UPDATE_STATUS, true, this);
    }

    private void startUpdatingDetails(){
        if(timer == null){
            timer = new Timer();
            timer.scheduleAtFixedRate(new AmbulanceDetailTimerTask(), Const.DELAY, Const.TIME_SCHEDULE);
        }
    }

    private class AmbulanceDetailTimerTask extends TimerTask{
        @Override
        public void run() {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setData();
                }
            });
        }
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        super.onTaskCompleted(response, serviceCode);
        switch (serviceCode){
            case Const.ServiceCode.UPDATE_STATUS:
                Log.i("updateStaus","Response-->>>" + response);
                AndyUtils.removeCustomProgressDialog();
                if(pContent.isSuccess(response)){
                    activity.startUpdatingAmbulanceDetail();
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case Const.PERMISSION_CALL:
                if(grantResults.length > 0){
                    if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                        makeACall();
                    }
                }
                break;
        }
    }

    private void stopUpdatingDetails(){
        if(timer != null){
            timer.cancel();
            timer = null;
        }
    }

    @Override
    public void onStop() {
        stopUpdatingDetails();
        super.onStop();
    }
}

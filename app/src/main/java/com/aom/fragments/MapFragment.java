package com.aom.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

import com.aom.R;
import com.aom.models.AmbulanceDetail;
import com.aom.utils.AndyUtils;
import com.aom.utils.Const;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Timer;
import java.util.TimerTask;

public class MapFragment extends BaseFragment implements OnMapReadyCallback{
    private SupportMapFragment mapFragment;
    private GoogleMap map;
    private float currentZoom = -1;
    private Timer timer;
    private boolean isFirstCall = true;
    private Marker marker;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
                                                              View view = inflater.inflate(R.layout.fragment_map, container, false);
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.fragmentMap);
        if(activity.ambulanceDetail != null) {
            setUpMap();
        }
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void setUpMap(){
        if(map == null) {
            AndyUtils.showCustomProgressDialog(activity, false);
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                activity.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, Const.PERMISSION_LOCATION);
            }
        }
        else {
            map = googleMap;
            map.setMyLocationEnabled(false);
            map.getUiSettings().setMyLocationButtonEnabled(false);
            map.getUiSettings().setZoomControlsEnabled(false);
            map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition cameraPosition) {
                    if(currentZoom == -1){
                        currentZoom = cameraPosition.zoom;
                    }
                    else if(cameraPosition.zoom != currentZoom){
                        currentZoom = cameraPosition.zoom;
                    }
                }
            });
            map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    startUpdatingAmbulanceLocation();
                }
            });
        }
    }

    private void startUpdatingAmbulanceLocation(){
        if(timer == null){
            timer = new Timer();
            timer.scheduleAtFixedRate(new LocationUpdateTimerTAsk(), Const.DELAY, Const.TIME_SCHEDULE);
        }
    }

    private class LocationUpdateTimerTAsk extends TimerTask{
        @Override
        public void run() {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showAmbulanceLocation();
                }
            });
        }
    }

    private void showAmbulanceLocation(){
        if(isFirstCall) {
            MarkerOptions markerOptions = createMarker(activity.ambulanceDetail);
            marker = map.addMarker(markerOptions);
            animateCameraToMarker(new LatLng(activity.ambulanceDetail.getCurrentLat(), activity.ambulanceDetail.getCurrentLng()), true);
        }
        else{
            Location location = new Location("");
            location.setLatitude(activity.ambulanceDetail.getCurrentLat());
            location.setLongitude(activity.ambulanceDetail.getCurrentLng());
            marker.setPosition(new LatLng(activity.ambulanceDetail.getCurrentLat(), activity.ambulanceDetail.getCurrentLng()));
            marker.setPosition(new LatLng(activity.ambulanceDetail.getCurrentLat(), activity.ambulanceDetail.getCurrentLng()));
            animateMarker(marker, new LatLng(activity.ambulanceDetail.getCurrentLat(), activity.ambulanceDetail.getCurrentLng()), location, false);
            animateCameraToMarker(new LatLng(activity.ambulanceDetail.getCurrentLat(), activity.ambulanceDetail.getCurrentLng()), true);
        }
    }

    private MarkerOptions createMarker(AmbulanceDetail ambulanceDetail){
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.flat(true);
        markerOptions.anchor(0.7f, 0.7f);
        markerOptions.position(new LatLng(ambulanceDetail.getCurrentLat(), ambulanceDetail.getCurrentLng()));
//        if((ambulanceDetail.getSpeed() * 60 * 60 / 1000) > 80 && (ambulanceDetail.getSpeed() * 60 * 60 / 1000) < 120) {
//            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.yellow_pin));
//        }
//        else if((ambulanceDetail.getSpeed() * 60 * 60 / 1000) >= 120){
//            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.red_pin));
//        }
//        else{
//            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.green_pin));
//        }
//        if(ambulanceDetail.getIsOnline().equalsIgnoreCase("NO")){
//            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.grey_pin));
//        }
        return markerOptions;
    }

    public void animateCameraToMarker(LatLng latLng, boolean isAnimate) {
        Log.i("mapFragment","animateCameraToMarker called");
        try {
            CameraUpdate cameraUpdate;
            cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 17);
            if (map != null) {
                Log.i("mapFragment","animateCameraToMarker if called");
                if (isAnimate)
                    map.animateCamera(cameraUpdate);
                else
                    map.moveCamera(cameraUpdate);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void animateMarker(final Marker marker, final LatLng toPosition,
                                 final Location toLocation, final boolean hideMarker) {
        if (map == null || !this.isVisible()) {
            return;
        }
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection projection = map.getProjection();
        Point startPoint = projection.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = projection.fromScreenLocation(startPoint);
        final double startRotation = marker.getRotation();
        final long duration = 500;

        final Interpolator interpolator = new LinearInterpolator();
        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));
                float rotation = (float) (t * toLocation.getBearing() + (1 - t)
                        * startRotation);
                if (rotation != 0) {
                    marker.setRotation(rotation);
                }
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }

    @Override
    public void onStop() {
        if(timer != null){
            timer.cancel();
            timer = null;
        }
        super.onStop();
    }
}

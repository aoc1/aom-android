package com.aom.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.aom.BaseActivity;
import com.aom.MapActivity;
import com.aom.parse.AsyncTaskCompleteListener;
import com.aom.parse.ParseContent;
import com.aom.utils.PreferenceHelper;

public class BaseFragment extends Fragment implements AsyncTaskCompleteListener {

    public MapActivity activity;
    public PreferenceHelper pHelper;
    public ParseContent pContent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if(getActivity() instanceof MapActivity)
            activity = (MapActivity) getActivity();

        pHelper = new PreferenceHelper(activity);
        pContent = new ParseContent(activity);
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {

    }
}

package com.aom.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceHelper {

    private SharedPreferences app_prefs;
    private final String USER_ID = "user_id";
    private final String SESSION_TOKEN = "session_token";
    private final String EMAIL = "email";
    private final String DEVICE_TOKEN = "device_token";
    private final String LOGIN_TYPE = "login_type";
    private final String REQUEST_ID = "request_id";
    private final String QR_CODE_LOGIN = "qr_code_login";
    public PreferenceHelper(Context context){
        app_prefs = context.getSharedPreferences(Const.PREF_NAME,
                Context.MODE_PRIVATE);
    }

    public void putUserId(int userId){
        SharedPreferences.Editor editor = app_prefs.edit();
        editor.putInt(USER_ID, userId);
        editor.apply();
    }

    public int getUserId(){
        return app_prefs.getInt(USER_ID, 0);
    }

    public void putSessionToken(String sessionToken){
        SharedPreferences.Editor editor = app_prefs.edit();
        editor.putString(SESSION_TOKEN, sessionToken);
        editor.apply();
    }

    public String getSessionToken(){
        return app_prefs.getString(SESSION_TOKEN, null);
    }

    public void putUserEmail(String email){
        SharedPreferences.Editor editor = app_prefs.edit();
        editor.putString(EMAIL, email);
        editor.apply();
    }

    public String getUserEmail(){
        return app_prefs.getString(EMAIL, null);
    }

    public void putDeviceToken(String deviceToken) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(DEVICE_TOKEN, deviceToken);
        edit.apply();
    }

    public String getDeviceToken() {
        return app_prefs.getString(DEVICE_TOKEN, null);

    }

    public void putLoginType(int type){
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putInt(LOGIN_TYPE, type);
        edit.apply();
    }

    public int getLoginType(){
        return app_prefs.getInt(LOGIN_TYPE, 0);
    }

    public void putRequestId(int reqId){
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putInt(REQUEST_ID, reqId);
        edit.apply();
    }

    public int getRequestId(){
        return app_prefs.getInt(REQUEST_ID, 0);
    }

    public void putQRCodeLogin(int QRLogin){
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putInt(QR_CODE_LOGIN, QRLogin);
        edit.apply();
    }

    public int getQRCodeLogin(){
        return app_prefs.getInt(QR_CODE_LOGIN, 0);
    }
}

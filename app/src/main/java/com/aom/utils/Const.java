package com.aom.utils;

public class Const {

    //API Key
    public static final String BROWSER_KEY = "AIzaSyD5YMDzxaYBEqcwoBL1vZEAlYN6cB15nJA";
    public static final String SENDER_ID = "1003049675554";
    public static final String SERVER_API_KEY = "AIzaSyAurV37kP-7wCfZtWdFnAYXNFPF04JJ2eE";
    public static final String FIREBASE_KEY = "1:1003049675554:android:54a9afb56e0f3d80";

    //FRAGMENT TAGS
    public static final String FRAGMENT_AMBULANCE = "fragment_ambulance";
    public static final String FRAGMENT_MAP = "fragment_map";
    public static final String FRAGMENT_HOME = "fragment_home";
    public static final String FRAGMENT_AMBULANCE_IMAGES = "fragment_ambulance_images";
    public static final String FRAGMENT_SETTING = "fragment_setting";
    public static final String FRAGMENT_AMBULANCE_CRASH_LOCATION = "fragment_ambulance_map";
    public static final String FRAGMENT_IMAGE = "fragment_image";

    //GENERAL
    public static final String DEVICE_TYPE = "android";
    public static final String TAG = "AOM";
    public static final String PREF_NAME = "Ambulance Operation Center";
    public static final String URL = "url";
    public static final String TYPE_REF = "REF";
    public static final String TYPE_REF_OUT = "REFER OUT";
    public static final String TYPE_EMS = "ems";
    public static final String TYPE_ALL = "all";
    public static final String TYPE_STB = "stb";
    public static final String KEY_RESULTS = "results";
    public static final String KEY_FORMATTED_ADDRESS = "formatted_address";
    public static final String GEO_CODE_API = "http://maps.googleapis.com/maps/api/geocode/json?";
    public static boolean IS_FIRST_SERVICE_CALL = true;


    //Constants
    public static final int PERMISSION_LOCATION = 1;
    public static final int PERMISSION_CALL = 2;
    public static final int DELAY = 0 * 1000;
    public static final int TIME_SCHEDULE = 2 * 1000;
    public static final int TIME_SCHEDULE_REQUEST = 4 * 1000;
    public static final double DEFAULT_LAT = 16.3091198;
    public static final double DEFAULT_LNG = 102.883866;

    public class ServiceType{
//        public static final String HOST_URL = "http://aoc.tely360.com/aoc/";
//        public static final String HOST_URL = "http://192.168.0.104/aoc/";
//        public static final String HOST_URL = "http://aoc.tely360.com/aoc-beta1/";
        public static final String HOST_URL = "http://eoc.tely360.com/aoc-beta/";
        public static final String BASE_URL = HOST_URL + "public/";
        public static final String GET_AMBULANCES = BASE_URL + "ambulances";
//        public static final String LOGIN = BASE_URL + "app_login";
        public static final String LOGIN = BASE_URL + "aom_login";
        public static final String GET_NOTIFICATION = BASE_URL + "notification_list";
        public static final String GET_AMBULANCE_IMAGE = BASE_URL + "ambulances?";
        public static final String CHECK_AMBULANCE_STATUS = BASE_URL + "aom_check_request";
        public static final String UPDATE_STATUS = BASE_URL + "requestCompleted?";
        public static final String ADD_STAFF = BASE_URL + "requestAddStaff?";
    }

    public class ServiceCode{
        public static final int GET_AMBULANCES = 1;
        public static final int LOGIN = 2;
        public static final int GET_NOTIFICATION = 3;
        public static final int GET_AMBULANCE_IMAGE = 4;
        public static final int GEO_CODE_API = 5;
        public static final int DRAW_PATH = 6;
        public static final int CHECK_AMBULANCE_STATUS = 7;
        public static final int UPDATE_STATUS = 8;
        public static final int ADD_STAFF = 9;
    }

    public class Params{
        public static final String PICTURE = "picture";
        public static final String EMAIL = "email";
        public static final String PASSWORD = "password";
        public static final String AMBULANCE_ID = "ambID";
        public static final String TOKEN = "token";
        public static final String ID = "id";
        public static final String DEVICE_TYPE = "device_type";
        public static final String LATITUDE = "latitude";
        public static final String LONGITUDE = "longitude";
        public static final String DEVICE_TOKEN = "device_token";
        public static final String DOCTOR = "doctor";
        public static final String LOCATION = "location";
        public static final String TYPE = "type";
        public static final String REQUEST_ID = "request_id";
        public static final String HOSPITAL_ID = "hospital_id";
        public static final String STAFF = "staff";
    }
}

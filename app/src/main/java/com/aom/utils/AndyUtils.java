package com.aom.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.ContextCompat;
import android.view.Window;
import android.widget.Toast;

import com.aom.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Elluminati elluminati.in
 */
@SuppressLint("NewApi")
public class AndyUtils {
	private static ProgressDialog mProgressDialog;
	private static Dialog mDialog;
	private static OnProgressCancelListener progressCancelListener;

	public static void removeSimpleProgressDialog() {
		try {
			if (mProgressDialog != null) {
				if (mProgressDialog.isShowing()) {
					mProgressDialog.dismiss();
					mProgressDialog = null;
				}
			}
		} catch (IllegalArgumentException ie) {
			ie.printStackTrace();
		}

	}

	public static boolean isNetworkAvailable(Activity activity) {
		ConnectivityManager connectivity = (ConnectivityManager) activity
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity == null) {
			return false;
		} else {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) {
				for (int i = 0; i < info.length; i++) {
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public static boolean eMailValidation(String emailstring) {
		if (null == emailstring || emailstring.length() == 0) {
			return false;
		}
		Pattern emailPattern = Pattern
				.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
						+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
		Matcher emailMatcher = emailPattern.matcher(emailstring);
		return emailMatcher.matches();
	}

	public static void showToast(String msg, Context ctx) {
		Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
	}

	public static void showCustomProgressDialog(Context context,boolean isCancelable) {
		if (mDialog != null && mDialog.isShowing())
			return;
		AndyUtils.progressCancelListener = progressCancelListener;

		mDialog = new Dialog(context, R.style.MyDialog);
		mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mDialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(ContextCompat.getColor(context, R.color.color_black_transparent)));
		mDialog.setContentView(R.layout.progressbar);

		if(!((Activity)context).isFinishing())
			mDialog.show();
	}

	public static void removeCustomProgressDialog() {
		try {
			AndyUtils.progressCancelListener = null;
			if (mDialog != null) {
//				if (mProgressDialog.isShowing()) {
					mDialog.dismiss();
					mDialog = null;
//				}
			}
		} catch (IllegalArgumentException ie) {
			ie.printStackTrace();
		}
	}

	public interface OnProgressCancelListener{
		public void onCancel();
	}
}

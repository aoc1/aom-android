package com.aom;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;

import com.aom.utils.PreferenceHelper;
import com.google.android.gcm.GCMBaseIntentService;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * IntentService responsible for handling GCM messages.
 */
public class GCMIntentService extends GCMBaseIntentService {

	private int uniqueId = 0, requestId;
	private JSONObject jsonObject;
	private String TAG = "GCM";


	public GCMIntentService() {
		super(CommonUtilities.SENDER_ID);
		Log.i(TAG,"GCMIntentService constructor called");
	}

	@Override
	public void onCreate() {
		Log.i(TAG,"GCMIntentService onCreate called");
		super.onCreate();
	}

	@Override
	protected void onRegistered(Context context, String registrationId) {
		Log.i(TAG,"onRegistered called");
		CommonUtilities.displayMessage(context, "Device registered");
		new PreferenceHelper(context).putDeviceToken(registrationId);
		Log.i(TAG, registrationId);
		publishResults(registrationId, Activity.RESULT_OK);
	}

	@Override
	protected void onUnregistered(Context context, String registrationId) {
		Log.i(TAG,"onUnRegistered called");
		CommonUtilities.displayMessage(context, "Device Unregistered");
	}

	@Override
	protected void onMessage(Context context, Intent intent) {
		String team = null;
		Log.i(TAG, intent.getExtras() + "");
		String message = intent.getExtras().getString("message");
//		String team = intent.getExtras().getString("team");
		Log.i(TAG, "onMessage push message:" + message);
//		Log.i(TAG, "onMessage push Response***********:" + team);

		if(message != null){
			try {
				jsonObject = new JSONObject(message);
				JSONObject object = jsonObject.getJSONObject("MESSAGE");
				team = object.getString("TEAM");
				uniqueId = object.getInt("unique_id");

			} catch (JSONException e) {
				e.printStackTrace();
			}
			generateNotification(context, message, team);
		}
	}

	@Override
	protected void onDeletedMessages(Context context, int total) {
		Log.i(TAG, "onDeleteMessage called");
		String message = "message deleted " + total;
		CommonUtilities.displayMessage(context, message);
	}

	@Override
	public void onError(Context context, String errorId) {
		Log.i(TAG, "onError called");
	}

	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		Log.i(TAG, "onRecoverableError called");
		return super.onRecoverableError(context, errorId);
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@SuppressWarnings("deprecation")
	private void generateNotification(Context context, String message,String team)
	{
		Log.i(TAG, "generateNotification called");
		String title = context.getString(R.string.app_name);
		int icon = R.mipmap.ic_launcher;
		long when = System.currentTimeMillis();

		Uri alarmSound = RingtoneManager
				.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);

		Notification notification = new Notification(icon, message, when);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notification.defaults |= Notification.DEFAULT_SOUND;
		notification.defaults |= Notification.DEFAULT_VIBRATE;
		notification.flags |= Notification.FLAG_SHOW_LIGHTS;
		notification.ledARGB = 0x00000000;
		notification.ledOnMS = 0;
		notification.ledOffMS = 0;

		Intent notificationIntent = new Intent(context, MapActivity.class);
		notificationIntent.putExtra("fromNotification", "notification");
//		notificationIntent.putExtra(Const.EXTRA_WALKER_STATUS, team);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);

		Notification.Builder builder = new Notification.Builder(context);
		builder.setSound(alarmSound);

		Log.i(TAG,"uniqueId********" + uniqueId);

		switch(uniqueId){


			default:
				break;
		}

		PendingIntent pendingIntent = PendingIntent.getActivity(context, uniqueId,
				notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

		builder.setContentIntent(pendingIntent)
				.setSmallIcon(R.mipmap.ic_launcher)
				.setTicker(context.getResources().getString(
								R.string.app_name))
				.setWhen(System.currentTimeMillis())
				.setAutoCancel(true).setContentTitle(title)
				.setContentText(team);

		notification = builder.build();

		sendBroadcast(notificationIntent);
		notificationManager.notify(uniqueId, notification);

		PowerManager pm = (PowerManager) context
				.getSystemService(Context.POWER_SERVICE);
		PowerManager.WakeLock wakeLock = pm.newWakeLock(
				PowerManager.FULL_WAKE_LOCK
						| PowerManager.ACQUIRE_CAUSES_WAKEUP
						| PowerManager.ON_AFTER_RELEASE, "WakeLock");
		wakeLock.acquire();
		wakeLock.release();
	}

	private void publishResults(String regid, int result) {
		Log.i(TAG,"publishResults called");
		Intent intent = new Intent(CommonUtilities.DISPLAY_REGISTER_GCM);
		intent.putExtra(CommonUtilities.RESULT, result);
		intent.putExtra(CommonUtilities.REGID, regid);
		sendBroadcast(intent);
	}
}